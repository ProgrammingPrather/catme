CREATE TABLE [accounts](
	[firstname] [varchar](255) NOT NULL,
	[lastname] [varchar](255) NOT NULL,
	[address1] [varchar](255) NOT NULL,
	[address2] [varchar](255) NULL,
	[city] [varchar](255) NOT NULL,
	[state] [varchar](255) NOT NULL,
	[zipcode] [varchar](255) NOT NULL,
	[username] [varchar](255) NOT NULL,
	[password1] [varchar](255) NOT NULL,
	[password2] [varchar](255) NOT NULL
) ON [PRIMARY]