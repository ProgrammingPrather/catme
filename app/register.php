<! php for method and error of register fields below-->
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$iserror = false;
	if (empty($_POST["fname"])) {
		$iserror = true;
		$fnameErr = "First Name is Required";
	}
	if (empty($_POST["lname"])) {
		$iserror = true;
		$lnameErr = "Last Name is Required";
	}
	if (empty($_POST["addr1"])) {
		$iserror = true;
		$addr1Err = "Address1 is Required";
	}
	if (empty($_POST["city"])) {
		$iserror = true;
		$cityErr = "City is Required";
	}
	if (empty($_POST["state"])) {
		$iserror = true;
		$stateErr = "State is Required";
	}
	if (empty($_POST["zip"])) {
		$iserror = true;
		$zipErr = "Zipcode is Required";
	}
	if (empty($_POST["usern"])) {
		$iserror = true;
		$usernErr = "Username is Required";
	}
	if (empty($_POST["passw1"])) {
		$iserror = true;
		$passw1Err = "Password is Required";
	}
	if ($_POST["passw1"] != $_POST["passw2"]) {
		$passw2err = "You entered two different passwords";
		$iserror = true;
	}
	if ($iserror == false) {
		$servername = "localhost";
        $username = "root";
        $password = "owaspbwa";
        $dbname = "catme";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $sql = "INSERT INTO accounts (fname,lname,uname,pword,address,address2,city,state,zipcode)
        VALUES ('".$_POST['fname']."','".$_POST['lname']."','".$_POST['usern']."','".$_POST['passw1']."','".$_POST['addr1']."','".$_POST['address2']."','".$_POST['city']."','".$_POST['state']."','".$_POST['zip']."')";

        if (mysqli_query($conn, $sql)) {
            header('Location: login.php');
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

        mysqli_close($conn);
	}
}
?>

<! Html below-->
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="content/home-navbar.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!--Register: google "cat favicon" and research how to add a fav icon to your webpages. 
make form fields match length. -->
<!Navbar below-->
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php">Catme</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li class="active"><a href="register.php">Register</a></li>
            <li><a href="login.php">Login</a></li>
         </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!Page below-->
<div class="container">
    <div class="row">
        <form action="register.php" method="post">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="First name">First name</label> <input class="form-control" type="text" name="fname" placeholder="First name"> <?=$fnameErr?><br>
                </div>    
            </div>        
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Last name">Last name</label> <input class="form-control" type="text" name="lname" placeholder="Last name"><?=$lnameErr?><br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Address1">Address1</label> <input class="form-control" type="text" name="addr1" placeholder="Address1"><?=$addr1Err?><br>
                </div>   
            </div>
            <div class="col-sm-4">
                <div class="form-group"> 
                    <label for="Address2">Address2</label> <input class="form-control" type="text" name="address2" placeholder="Address2"><br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">    
                    <label for"City">City</label> <input class="form-control" type="text" name="city"placeholder="City"> <?=$cityErr?><br>
                </div>    
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for"State">State</label> <input class="form-control" type="text" name="state"placeholder="State"> <?=$stateErr?><br>
                </div> 
            </div>
            <div class="col-sm-4">
                <div class="form-group"> 
                    <label for "Zipcode">Zipcode</label> <input class="form-control" type="text" name="zip"placeholder="Zipcode"> <?=$zipErr?><br>
                </div>   
            </div>
            <div class="col-sm-4">
                <div class="form-group"> 
                    <label for"Username">Username</label> <input class="form-control" type="text" name="usern"placeholder="Username"> <?=$usernErr?><br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">    
                    <label for"Password">Password</label> <input class="form-control" type="text" name="passw1"placeholder="Password"> <?=$passw1Err?><br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">    
                    <label for"Re-enter Password">Re-enter Password</label> <input class="form-control" type="text" name="passw2"placeholder="Re-enter Password"> <?=$passw2err?><br>
                </div>    
            </div>
            <input type="submit" value="Submit">
        </form>
    </div>	
</div>
<!javascript script goes here-->
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
