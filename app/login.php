<?php
//log in  below 
if ($_SERVER['REQUEST_METHOD'] === 'POST') { //if request is identical to post
	  $iserror = false;
	  if (empty($_POST["username"])) {
	    	$iserror = true;
		    $usernameErr = "Username is Required";
	  }
  	if (empty($_POST["password"])) {
	    	$iserror = true;
	    	$passwordErr = "Password is Required";
    }
    if ($iserror == false) {
        echo $_GET['username'];
		    $servername = "localhost";
		    $username = "root";
		    $password = "owaspbwa";
		    $dbname = "catme";
		
		    // Create connection
		    $conn = mysqli_connect($servername, $username, $password, $dbname);
		    // Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}
		
		    $sql = "select * from accounts where uname='".$_POST['username']."' and pword='".$_POST['password']."'";
		
		    $result = mysqli_query($conn, $sql);
		
		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    $row = mysqli_fetch_assoc($result);
        //next query
        //echo 'The name is ' . $row['uname'];
        $name = $row['uname'];
        $token = '';
        for ($x = 0; $x <= strlen($name)-1; $x++) {
            //echo "The number is: $x <br>";
            $token .= substr($name,$x,1);
            $token .= 'x';
        } 
        //echo 'The token is ' . $token;
        //echo var_dump($row);
        $sql = "insert into token values ('$name','$token')"; 
		     if (mysqli_query($conn, $sql)) {
           //set cookie
           setcookie('catmecookie',$token);
            header('Location: userhome.php');
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
		} else {
		    $non_match_err='Username or Password is Incorrect';
		}
		
		mysqli_close($conn);
    }
}

?>   
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html>
<head>	
	<link rel="stylesheet" type="text/css" href="homestyle.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
 <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php">Catme</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li><a href="register.php">Register</a></li>
            <li class="active"><a href="login.php">Login</a></li>
         </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<div class="container">
  <div class="row">
  	<form action="login.php" method="post">
	    <fieldset><legend>Log In to Catme</legend>
        <div class="col-sm-4">
          <div class="form-group">
			      <label for="username">Username: </label> 
            <input class="form-control" type="text" name="username" id="username" placeholder="Username"><?=$usernameErr?></br>
		      </div>
        </div>  
        <div class="col-sm-4">
          <div class="form-group">	
            <label for="password">Password: </label>
            <input class="form-control" type="password" name="password" id="password" placeholder="Password"><?=$passwordErr?></br>
		    	</div>
          </div>
        <input type="submit" value="Login" id="sub">
        <?php echo $non_match_err; ?>
		  </fieldset>	
  	</form>
  </div>
</div>
<!javascript script goes here-->
  <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
